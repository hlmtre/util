set nocompatible

set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
"filetype plugin indent off

map <C-l> :tabn<CR>
map <C-h> :tabp<CR>
colorscheme ron
 
syntax on
set number
set hlsearch
set incsearch
"set nowrap
set autoindent
"set si " smart indent
set history=1000
"set cursorline
set expandtab
set shiftwidth=2
set tabstop=2
set ruler
set showcmd
"set ai

"set background=dark
"colorscheme wombat
