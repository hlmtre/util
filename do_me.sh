#!/bin/bash
# automatic setup of my necessaries (some of them)

echo "Making your life easier..."
git clone https://hlmtre@bitbucket.org/hlmtre/util.git
cd util
if [ -f ~/.vimrc ]; then
  mv ~/.vimrc ~/.vimrc.bak
fi
cp .vimrc ~/.vimrc
cp .bashrc ~/.bashrc
cp .zshrc ~/.zshrc
echo "Done!"
