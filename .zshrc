# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="hlmtre"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable weekly auto-update checks
DISABLE_AUTO_UPDATE="true"

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git debian)

source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/home/hlmtre/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/home/hlmtre/os_340/sys161/bin:/home/hlmtre/os_340/sys161/tools/bin:
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd beep extendedglob nomatch notify
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/hlmtre/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
alias lminus22='ssh -p 117 hlmtre@192.168.1.105'
alias iminus22='ssh -p 117 hlmtre@hellmitre.homelinux.org'
alias 311='cd /home/hlmtre/Documents/chico/F10/311'
alias asu='aptitude update && aptitude safe-upgrade'
alias lucid='ssh hlmtre@192.168.1.121'
alias ls='ls --color=auto'
alias m='/usr/local/bin/ncmpcpp'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias alpine='alpine -sort reverse'
alias pine='alpine -sort reverse'
alias i7='ssh hellmitre@infinite7.homelinux.org'
alias mm='sshfs -p 117 hlmtre@192.168.1.105:/ /home/hlmtre/remote'
alias fu='fusermount -u /home/hlmtre/remote'
alias jaguar='ssh hlmtre@jaguar.ecst.csuchico.edu'
alias hhorg='ssh hlmtre@hellmitre.homelinux.org'
alias alpine='alpine -z'
alias vi='vim'
alias deathstar='ssh -p 8583 hlmtre@173.230.250.131'
alias gpom='git push origin master'
alias rackmount='ssh hlmtre@zero9f9.com'
VISUAL=vim; export VISUAL
EDITOR=vim; export EDITOR
#PROMPT='%T/%n@%m[%~]
#:w$ ' 
#keychain ~/.ssh/id_rsa
#. ~/.keychain/noisycricket-sh
#eval `keychain --eval --nogui -Q -q id_rsa`

#if [ "$PS1" ] ; then 
#mkdir -m 0700 /dev/cgroup/cpu/user/$$
#echo $$ > /dev/cgroup/cpu/user/$$/tasks
#fi

# stuff for distcc
#PATH="/usr/local/distcc/bin:${PATH}"
#DISTCC_HOSTS="192.168.1.121 192.168.1.105 localhost"
export SSH_AUTH_SOCK
export GNOME_KEYRING_SOCKET

#bindkey -v
#let "flowers = $RANDOM % 25";echo -en "\nI should"; if [[ $flowers != 0 ]]; then echo -n " not";fi;echo -e " buy flowers today.\n\n"

#PATH=$PATH:$HOME/.rvm/bin:/home/hlmtre/android-sdk-linux/platform-tools # Add RVM to PATH for scripting
# create the pane with irssi's nicklist
function irssi_nickpane() {
	tmux renamew irssi                                              # name the window
	tmux -q setw main-pane-width $(( $(tput cols) - 21))            # set the main pane width to the total width-20
	tmux splitw -v "cat ~/.irssi/nicklistfifo"                      # create the window and begin reading the fifo
	tmux -q selectl main-vertical                                   # assign the layout
	tmux selectw -t irssi                                           # select window 'irssi'
	tmux selectp -t 0                                               # select pane 0
}

# repair running irssi's nicklist pane
function irssi_repair() {
	tmux selectw -t irssi
	tmux selectp -t 0
	tmux killp -a                                                   # kill all panes
	irssi_nickpane
}
