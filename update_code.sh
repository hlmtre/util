#!/bin/bash
# update all the git repositories in the code directory. also recurses into 'com' and updates that.

update_directory ()
{
  if [ "$1" ]
  then
    repo=$1
    echo "$(tput setaf 2) Updating $repo... $(tput sgr0)"
    cur_branch=`git symbolic-ref HEAD 2>/dev/null | cut -d"/" -f 3` # get the name of the current branch
    git stash -u # store all tracked and untracked files for restoration post-update
    git checkout master
    retval=$? # return code
    if [ $retval -ne 0 ]; then
      echo "$(tput setaf 1) Something's wrong with checkout. Aborting... $(tput sgr0)"
      exit 1
    fi
    git pull origin
    retval=$? # return code
    if [ $retval -ne 0 ]; then
      echo "$(tput setaf 1) Something's wrong with merging. Aborting... $(tput sgr0)"
      exit 1
    fi
    git checkout $cur_branch
    git stash pop # restore current untracked and uncommitted content
  else
    echo "KABOOOM"
    exit 1
  fi
}

# this is 'main'

for dir in /home/mdw/tcv/code/*/
do
  cd $dir
  # com submodule
  if [ -d "com" ]; then
    cd com
    update_directory $dir/com # though $dir does have trailing slashes, the filesystem doesn't care
  fi
  update_directory $dir
done
